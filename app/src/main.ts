const worker = new Worker("./worker.js");
worker.onmessage = e => {
    const h2 = document.createElement("h2");
    h2.textContent = e.data;
    document.body.appendChild(h2);
};
worker.postMessage("");