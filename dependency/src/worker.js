export default {
    init: function() {
        self.onmessage = () => {
            postMessage("Work hard, play hard!")
        };
    }
};